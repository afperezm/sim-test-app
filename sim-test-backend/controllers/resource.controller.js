const db = require("../models/resource.model");

module.exports = {
    findAll: function (req, res) {
        db.find(function (err, resources) {
            if (err) {
                console.log(err);
            } else {
                res.json(resources);
            }
        });
    },
    findById: function (req, res) {
        let id = req.params.id;
        db.findById(id, function (err, resource) {
            res.json(resource);
        });
    },
    create: function (req, res) {
        let resource = new db(req.body);
        resource.save()
            .then(resource => {
                res.status(200).json({'resource': 'resource added successfully'});
            })
            .catch(err => {
                res.status(400).send('adding new resource failed');
            });
    },
    update: function (req, res) {

    },
    remove: function (req, res) {
        let id = req.params.id;
        db.findByIdAndRemove(id, function (err, resource) {
            if (err) return res.status(500).send(err);
            const response = {
                message: "Resources successfully deleted",
                id: resource._id
            };
            return res.status(200).send(response);
        });
    }
};
