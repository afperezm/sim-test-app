const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CoverageSchema = new Schema({
    startDate: { type: Date, default: Date.now },
    endDate: { type: Date, default: Date.now },
    latitude: { type: Number },
    longitude: { type: Number }
});

let ResourceSchema = new Schema({
    title: {type: String},
    keywords: [String],
    description: {type: String},
    source: {type: String},
    type: {type: Number},
    coverage: { type: CoverageSchema }
});

const Resource = mongoose.model('Resource', ResourceSchema);

module.exports = Resource;
