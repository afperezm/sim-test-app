const router = require('express').Router();
const resourceController = require("../../controllers/resource.controller");

// Matches with "/api/resources"
router.route("/")
    .get(resourceController.findAll)
    .post(resourceController.create);

// Matches with "/api/resources/:id"
router.route("/:id")
    .get(resourceController.findById)
    .put(resourceController.update)
    .delete(resourceController.remove);

module.exports = router;
