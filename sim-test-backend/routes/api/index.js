const router = require("express").Router();
const ResourceRoutes = require("./resource.route");

// Resource routes
router.use("/resources", ResourceRoutes);

module.exports = router;
