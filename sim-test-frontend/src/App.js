import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";

import CreateResource from "./components/create-resource.component";
import EditResource from "./components/edit-resource.component";
import ListResources from "./components/list-resource.component";

import logo from './logo.svg';
import { version } from '../package.json';

class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="container">
                <div className="navbar-header">
                    <a className="navbar-brand" href="/" target="_self">
                        <img className="img-fluid" width={100} src={logo} alt="Comisión de la Verdad" />
                        <span>&nbsp;SIM-Test</span>
                        <span className="navbar-version">&nbsp;v{version}</span>
                    </a>
                </div>
                <div className="collpase navbar-collapse">
                  <ul className="navbar-nav mr-auto">
                    <li className="navbar-item">
                      <Link to="/list-resources" className="nav-link">Lista de recursos</Link>
                    </li>
                    <li className="navbar-item">
                      <Link to="/create-resource" className="nav-link">Crear recurso</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
          </div>
          <div className="container">
            <Route path="/list-resources" exact component={ListResources} />
            <Route path="/edit-resource/:id" component={EditResource} />
            <Route path="/create-resource" component={CreateResource} />
          </div>
        </Router>
    );
  }
}

export default App;
