import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

const Resource = props => (
    <tr>
        <td>{props.resource.title}</td>
        <td>{props.resource.keywords ? props.resource.keywords : '-'}</td>
        <td>{props.resource.description}</td>
        <td>{props.resource.source}</td>
        <td>{props.resource.type}</td>
        <td>{props.resource.startDate ? props.resource.startDate : '-'}</td>
        <td>{props.resource.endDate ? props.resource.endDate : '-'}</td>
        <td>{props.resource.latitude ? props.resource.latitude : '-'}</td>
        <td>{props.resource.longitude ? props.resource.longitude : '-'}</td>
        <td>
            <Link to={"/edit-resource/" + props.resource._id}>Editar</Link>
        </td>
    </tr>
);

export default class ListResources extends Component {

    constructor(props) {
        super(props);
        this.state = {
            resources: []
        };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/api/resources/')
            .then(response => {
                // console.log(response.data);
                this.setState({resources: response.data});
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    resourcesList() {
        return this.state.resources.map(function (currentResource, i) {
            return <Resource resource={currentResource} key={i}/>;
        })
    }

    render() {
        return (
            <div>
                <h3>Lista de recursos documentales</h3>
                <table className="table table-striped" style={{marginTop: 20}}>
                    <thead>
                    <tr>
                        <th>Título</th>
                        <th>Palabras clave</th>
                        <th>Descripcion</th>
                        <th>Fuente</th>
                        <th>Tipo</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha final</th>
                        <th>Latitud</th>
                        <th>Longitud</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.resourcesList()}
                    </tbody>
                </table>
            </div>
        )
    }
}
