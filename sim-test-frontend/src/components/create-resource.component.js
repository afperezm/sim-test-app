import React, {Component} from 'react';
import axios from 'axios';

export default class CreateResource extends Component {

    constructor(props) {
        super(props);

        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeKeywords = this.onChangeKeywords.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onChangeSource = this.onChangeSource.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeStartDate = this.onChangeStartDate.bind(this);
        this.onChangeEndDate = this.onChangeEndDate.bind(this);
        this.onChangeLatitude = this.onChangeLatitude.bind(this);
        this.onChangeLongitude = this.onChangeLongitude.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            title: '',
            keywords: '',
            description: '',
            source: '',
            type: null,
            startDate: '',
            endDate: '',
            latitude: '',
            longitude: ''
        }
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    onChangeKeywords(e) {
        this.setState({
            keywords: e.target.value
        });
    }

    onChangeDescription(e) {
        this.setState({
            description: e.target.value
        });
    }

    onChangeSource(e) {
        this.setState({
            source: e.target.value
        });
    }

    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeStartDate(e) {
        this.setState({
            startDate: e.target.value
        });
    }

    onChangeEndDate(e) {
        this.setState({
            endDate: e.target.value
        });
    }

    onChangeLatitude(e) {
        this.setState({
            latitude: e.target.value
        });
    }

    onChangeLongitude(e) {
        this.setState({
            longitude: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        console.log(this.state);

        let newResource = {
            title: this.state.title,
            keywords: '',
            description: this.state.description,
            source: this.state.source,
            type: this.state.type,
            coverage: {
                startDate: '',
                endDate: '',
                latitude: this.state.latitude,
                longitude: this.state.longitude
            }
        };

        if (this.state.keywords)
            newResource.keywords = this.state.keywords.split(",");

        if (this.state.startDate)
            newResource.coverage.startDate = new Date(Date.parse(this.state.startDate)).toISOString().substring(0, 10);

        if (this.state.endDate)
            newResource.coverage.endDate = new Date(Date.parse(this.state.endDate)).toISOString().substring(0, 10);

        axios.post('http://localhost:4000/api/resources', newResource)
            .then(res => console.log(res.data))
            .catch(err => {if (err) console.error(err)});

        this.setState({
            title: '',
            keywords: '',
            description: '',
            source: '',
            type: null,
            startDate: '',
            endDate: '',
            latitude: '',
            longitude: ''
        })
    }

    render() {
        return (
            <div style={{marginTop: 10}}>
                <h3>Crear o editar recurso</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group row">
                        <label htmlFor="title" className="col-sm-2 col-form-label">Título: </label>
                        <div className="col-sm-10">
                            <input type="text"
                                   className="form-control"
                                   id="title"
                                   name="title"
                                   placeholder="Título"
                                   value={this.state.title}
                                   onChange={this.onChangeTitle}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="keywords" className="col-sm-2 col-form-label">Palabras clave: </label>
                        <div className="col-sm-10">
                            <input type="text"
                                   className="form-control"
                                   id="keywords"
                                   name="keywords"
                                   placeholder="Insert keywords as comma separated values"
                                   value={this.state.keywords}
                                   onChange={this.onChangeKeywords}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="description" className="col-sm-2 col-form-label">Descripción: </label>
                        <div className="col-sm-10">
                            <input type="text"
                                   className="form-control"
                                   id="description"
                                   name="description"
                                   placeholder="Descripción"
                                   value={this.state.description}
                                   onChange={this.onChangeDescription}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="source" className="col-sm-2 col-form-label">Fuente: </label>
                        <div className="col-sm-10">
                            <input type="text"
                                   className="form-control"
                                   id="source"
                                   name="source"
                                   placeholder="Fuente"
                                   value={this.state.source}
                                   onChange={this.onChangeSource}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="row">
                            <legend className="col-form-label col-sm-2 pt-0">Tipo:</legend>
                            <div className="col-sm-10">
                                <div className="form-check">
                                    <input type="radio"
                                           className="form-check-input"
                                           id="typeOne"
                                           name="typeOptions"
                                           value="0"
                                           checked={this.state.type === '0'}
                                           onChange={this.onChangeType}
                                    />
                                    <label className="form-check-label" id="typeOne">Testimonio</label>
                                </div>
                                <div className="form-check">
                                    <input type="radio"
                                           className="form-check-input"
                                           id="typeTwo"
                                           name="typeOptions"
                                           value="1"
                                           checked={this.state.type === '1'}
                                           onChange={this.onChangeType}
                                    />
                                    <label className="form-check-label" id="typeTwo">Informe</label>
                                </div>
                                <div className="form-check">
                                    <input type="radio"
                                           className="form-check-input"
                                           id="typeThree"
                                           name="typeOptions"
                                           value="2"
                                           checked={this.state.type === '2'}
                                           onChange={this.onChangeType}
                                    />
                                    <label className="form-check-label" id="typeThree">Caso</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="startDate" className="col-sm-2 col-form-label">Start date:</label>
                        <div className="col-sm-4">
                            <input type="date"
                                   className="form-control"
                                   id="startDate"
                                   name="startDate"
                                   value={this.state.startDate}
                                   onChange={this.onChangeStartDate}
                            />
                        </div>
                        <label htmlFor="coordsLat" className="col-sm-2">Latitude:</label>
                        <div className="col-sm-4">
                            <input type="number"
                                   className="form-control"
                                   id="coordsLat"
                                   name="coordsLat"
                                   step="0.000001"
                                   value={this.state.latitude}
                                   onChange={this.onChangeLatitude}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="startDate" className="col-sm-2 col-form-label">End date:</label>
                        <div className="col-sm-4">
                            <input type="date"
                                   className="form-control"
                                   id="endDate"
                                   name="endDate"
                                   value={this.state.endDate}
                                   onChange={this.onChangeEndDate}
                            />
                        </div>
                        <label htmlFor="coordsLat" className="col-sm-2">Longitude:</label>
                        <div className="col-sm-4">
                            <input type="number"
                                   className="form-control"
                                   id="coordsLon"
                                   name="coordsLong"
                                   step="0.000001"
                                   value={this.state.longitude}
                                   onChange={this.onChangeLongitude}
                            />
                        </div>
                    </div>

                    <div className="form-group row">
                        <div className="offset-sm-5 col-sm-2">
                            <input type="submit" value="Crear recurso" className="btn btn-primary btn-block"/>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
