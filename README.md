# SIM-Test-App

This repository contains code for the technical test to the position as backend and frontend developer at the "Comisión de la Verdad" for the development of the "Sistema de Información Misional".

## Requirements

The following are the requirements to install in order to run the app:

- Node.js 10.x
- MongoDB 4.x
- Nodemon 1.19.4 >=

## Database configuration

If a database does not exist, MongoDB creates the database when you first store data for that database. Therefore no further configuration steps are necessary to setup the database, you only need to be sure that MongoDB is correctly installed.

## Installation

The application is split in two directories for backend and frontend. Both of them are developed as Node.js applications, and as such the first thing to do is to install the required packages using the command:

`$ npm install`

To run the server you can either use:

`$ nodemon server`

or

`$ node server.js`

To run the client you need to execute the command:
 
`$ npm start`